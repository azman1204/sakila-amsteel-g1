<%@ page import="java.util.ArrayList, com.amsteel.sakila.models.Film" %>
<%@ include file="../include/header.jsp" %>

<div class="ibox">
    <div class="ibox-content">
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            <%
            int no = 1;
            ArrayList<Film> films = (ArrayList<Film>) request.getAttribute("films");
            for (Film film : films) {
            %>
                <tr>
                    <td><%= no++ %></td>
                    <td><%= film.getTitle() %></td>
                    <td>
                        <button filmId="<%= film.getFilmId() %>"
                         class="film-detail btn btn-sm btn-primary" data-toggle="modal" data-target="#my-modal">Detail</button>

                         <button filmId="<%= film.getFilmId() %>"
                         class="film-detail2 btn btn-sm btn-success" data-toggle="modal" data-target="#my-modal">Detail 2</button>
                    </td>
                </tr>
            <% } %>
            </tbody>
        </table>

        <div class="modal inmodal" id="my-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <i class="fa fa-clock-o modal-icon"></i>
                        <h4 class="modal-title" id="film-title"></h4>
                    </div>

                    <div class="modal-body">
                        <p id="film-description"></p>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {
    // onclick 2nd button
    $('.film-detail2').click(function() {
        let filmId = $(this).attr("filmId");
        // ajax request
        $.get('film?act=details2&filmId=' + filmId, function(data) {
            $('#film-description').html(data);
        });
    });

    // onclick first button
    $('.film-detail').click(function() {
        let filmId = $(this).attr("filmId");
        // ajax request
        $.get('film?act=details&filmId=' + filmId, function(data) {
            console.log(data);
            $('#film-title').text(data.title);
            $('#film-description').text(data.description);
        });
    });
});
</script>

<%@ include file="../include/footer.jsp" %>
