<%@ page import="com.amsteel.sakila.models.Film" %>
<%
Film film = (Film) request.getAttribute("film");
%>

<table class="table table-striped table-bordered">
    <tr>
        <td>Title</td>
        <td><%= film.getTitle() %></td>
    </tr>
    <tr>
        <td>Description</td>
        <td><%= film.getDescription() %></td>
    </tr>
</table>