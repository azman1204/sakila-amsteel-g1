package com.amsteel.sakila.controllers;

import com.amsteel.sakila.models.Film;
import com.amsteel.sakila.services.FilmService;
import com.google.gson.Gson;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet("/film")
public class FilmController extends HttpServlet {
    HttpServletRequest request;
    HttpServletResponse response;

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        request = req;
        response = res;
        String action = req.getParameter("act");
        //action = action == null ? "list" : action;
        if (action == null) {
            action = "list"; // set to default
        }

        switch(action) {
            case "list":
                list();
                break;
            case "details":
                details();
                break;
            case "details2":
                details2();
                break;
            default:
                list();
        }
    }

    // return html
    public void details2() throws ServletException, IOException {
        FilmService filmService = new FilmService();
        int filmId = Integer.parseInt(request.getParameter("filmId"));
        Film film = filmService.getOne(filmId);
        request.setAttribute("film", film);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/film/details.jsp");
        dispatcher.forward(request, response);
    }

    // return JSON
    public void details() throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        FilmService filmService = new FilmService();
        int filmId = Integer.parseInt(request.getParameter("filmId"));
        Film film = filmService.getOne(filmId);
        //out.print(film.getTitle());
        Gson gson = new Gson();
        String json = gson.toJson(film);
        response.setContentType("application/json");
        out.print(json);
    }

    public void list() throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/film/list.jsp");
        FilmService filmService = new FilmService();
        ArrayList<Film> films = filmService.findAll();
        System.out.println(films.toString());
        request.setAttribute("films", films);
        dispatcher.forward(request, response);
    }
}
