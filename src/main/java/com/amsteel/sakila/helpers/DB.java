package com.amsteel.sakila.helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DB {
    public static PreparedStatement getConnection(String sql) {
        PreparedStatement stmt = null;
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/sakila", "root", "Azman@1204");
            stmt = conn.prepareStatement(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return stmt;
    }
}
