package com.amsteel.sakila.services;

import com.amsteel.sakila.helpers.DB;
import com.amsteel.sakila.models.Film;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FilmService {
    public ArrayList<Film> findAll() {
        String sql = "SELECT * FROM film limit 20";
        PreparedStatement stmt = DB.getConnection(sql);
        ArrayList<Film> films = new ArrayList<>();
        try {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Film film = new Film();
                film.setFilmId(rs.getInt("film_id"));
                film.setTitle(rs.getString("title"));
                film.setDescription(rs.getString("description"));
                film.setReleaseYear(rs.getInt("release_year"));
                film.setLanguageId(rs.getInt("language_id"));
                films.add(film);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return films;
    }

    public Film getOne(int filmId) {
        String sql = "SELECT * FROM film WHERE film_id = " + filmId;
        PreparedStatement stmt = DB.getConnection(sql);
        Film film = new Film();
        try {
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                film.setFilmId(rs.getInt("film_id"));
                film.setTitle(rs.getString("title"));
                film.setDescription(rs.getString("description"));
                film.setReleaseYear(rs.getInt("release_year"));
                film.setLanguageId(rs.getInt("language_id"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return film;
    }

    public static void main(String[] args) {
        FilmService filmService = new FilmService();
        ArrayList<Film> arr = filmService.findAll();
        for(Film f : arr) {
            System.out.println(f.getFilmId());
        }

        Film film = filmService.getOne(2);
        System.out.println(film.getTitle());
    }
}
